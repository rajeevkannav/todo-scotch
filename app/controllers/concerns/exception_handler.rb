module ExceptionHandler
  # provide the more graceful 'included' method
  extend ActiveSupport::Concern

  included do
    rescure_from ActiveRecord::RecordNotFound do |e|
      json_response({message: e.message}, :not_found)
    end

    rescure_from ActiveRecord::RecordInvalid do |e|
      json_response({message: e.message}, :not_found)
    end

  end
end