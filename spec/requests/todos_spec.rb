require 'rails_helper'
RSpec.describe 'Todos API', type: :request do
  # initialize test data
  let!(:todos) {create_list(:todo, 20)}
  let!(:todo_id) {todos.first.id}

  # Test suite for GET /todos
  describe 'GET /todos' do
    before { get '/todos'}
    it 'returns todos' do
      expect(json).not_to be_empty
      expect(json.size).to eql (20)
    end
    it 'expect status code 200'  do
      expect(response).to have_http_status(200)
    end
  end


  # Test suite for GET /todos/:id
  describe 'GET /todos/:id' do
    before { get "/todos/#{todo_id}"}
    context 'when the record exists' do
      it 'returns the todo' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(todo_id)
      end
      it 'expect status code 200'  do
        expect(response).to have_http_status(200)
      end
    end
    context 'when the record does not exists' do
      let(:todo_id) { 100 }
      it 'returns the status code 404' do
        expect(response).to have_http_status(404)
      end
      it 'returns a not found message'  do
        expect(response.body).to match(/Couldn't find Todo/)
      end
    end
  end

  # Test Suite for POST /todos
  describe 'POST /todos ' do
    let(:valid_attributes) { {title: 'Learn Rajeev', created_by: 1} } #valid payload
    let(:invalid_attributes) { {title: 'Learn Rajeev'} } #invalid payload
    context 'when the request is valid' do
      before { post '/todos', params: valid_attributes}
      it 'creates a todo' do
        expect(json['title']).to eq('Learn Rajeev')
      end
      it 'returns the status code 201' do
        expect(response).to have_http_status(201)
      end
    end
    context 'when the request is invalid' do
      before { post '/todos', params: invalid_attributes }
      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end
      it 'returns a validations failure message' do
        expect(response.body)
            .to match(/Validation failed: Created by can't be blank/)
      end
    end
  end

  # Test Suite for PUT /todos/:id
  describe 'PUT /todos/:id ' do
    let(:valid_attributes) { {title: 'Learn Rajeev Update'} } #valid payload

    context 'when the record exists' do
      before { put "/todos/#{todo_id}", params: valid_attributes}
      it 'updates the record' do
        expect(response.body).to be_empty #TODO: this is doubtful
      end
      it 'returns the status code 204' do
        expect(response).to have_http_status(204)
      end
    end

    context 'when the record does not exists' do
      let(:todo_id) { 100 }
      before { put "/todos/#{todo_id}", params: valid_attributes}
      it 'returns the status code 404' do
        expect(response).to have_http_status(404)
      end
      it 'returns a not found message'  do
        expect(response.body).to match(/Couldn't find Todo/)
      end
    end
  end

  # Test Suite for DELETE /todos/:id
  describe 'DELETE /todos/:id' do
    before { delete "/todos/#{todo_id}"}
    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

  end
end